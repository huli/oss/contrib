package contrib
package io

import scalaprops._
import prelude._, std._, test._
import std.instances.all._

object S3Spec extends Scalaprops {

  object MyCfg extends svc.Env {
    lazy val S3_REGION            = getString("S3_REGION")
    lazy val S3_ACCESS_KEY_ID     = getString("S3_ACCESS_KEY_ID")
    lazy val S3_SECRET_ACCESS_KEY = getString("S3_SECRET_ACCESS_KEY")
  }

  val creds = S3Credentials(
    MyCfg.S3_REGION,
    MyCfg.S3_ACCESS_KEY_ID,
    MyCfg.S3_SECRET_ACCESS_KEY)

  val bucket = "test.contrib"

  object MyS3   extends S3("mys3", creds)
  object Bucket extends MyS3.Fragment[Int, String](bucket)

//======================================================================
  val spec                     = new prelude.io.DeviceSpec(MyS3)
  val q: S3.Query[Int, String] = S3.Query { case k -> v => k == 43 }

  val readWriteDeleteTest = Property.forAll { spec.run(spec.readWriteDelete) }
  val listTest            = Property.forAll { spec.run(spec.list(q))         }

//======================================================================
  val paginationTest = Property.forAll {
    val client   = new S3Client(creds)

    def elapsed(t0: Timestamp, t1: Timestamp) =
      (t0 duration t1 toMillis) / 1000.0

    val kvs      = range(1, 1001) map { i => i -> i.toString }
    val populate = Task.foreach(kvs) { case k -> v => Bucket insert (k, v) }

    val task1    = for {
      t0  <- Task0(Timestamp.now) & Entry.nop
      _   <- populate
      t1  <- Task0(Timestamp.now) & Entry.nop
    } yield (t0, t1)

    // XXX: comment in to run - takes a while
    val \/-((t0, t1)) = unsafeRunQuiet(task1)
    (elapsed(t0, t1) < 300.0) mustEqual true

    val task2    = for {
      t0  <- Task0(Timestamp.now) & Entry.nop
    //kvs <- Bucket.scan
      kvs <- client.parScan(bucket)
      t1  <- Task0(Timestamp.now) & Entry.nop
    } yield (kvs, t0, t1)

    // XXX: comment in to run - takes a while
    val \/-( (kvs1, t2, t3) ) = unsafeRunQuiet(task2)
    (elapsed(t2, t3) < 360.0) mustEqual true
  //kvs1.sorted               mustEqual kvs
    kvs1.map {case k -> v =>
      implicit val ev: data.Format = data.format.Json
      val k_ = net.netty.crypto.unHex(k) |> data.Data[Int].unpickle
      val v_ = data.Data[String].unpickle(v)
      k_ -> v_
    }.sorted mustEqual kvs

    ////////////////////////////////////////////////////////////////////
    val collect  = client.multilist(bucket) //Bucket.scan
    val \/-(ks0) = unsafeRunQuiet(collect)
    val ks       = ks0 map { k =>
      val bytes = net.netty.crypto.unHex(k)
      data.Data[Int].unpickle(bytes)(data.format.Json) } //XXX

    ks.length mustEqual 1001
    ks.sorted mustEqual kvs.map(fst)

  }.toProperties(
    "pagination",
    Param.timeout(11, java.util.concurrent.TimeUnit.MINUTES) )

} //S3Spec


//FIXME: make proper test instead of copy/paste
// this tests the key hexing / complex keys...
object S3Spec2 extends Scalaprops {

  object MyCfg extends svc.Env {
    lazy val S3_REGION            = getString("S3_REGION")
    lazy val S3_ACCESS_KEY_ID     = getString("S3_ACCESS_KEY_ID")
    lazy val S3_SECRET_ACCESS_KEY = getString("S3_SECRET_ACCESS_KEY")
  }

  val creds = S3Credentials(
    MyCfg.S3_REGION,
    MyCfg.S3_ACCESS_KEY_ID,
    MyCfg.S3_SECRET_ACCESS_KEY)

  val bucket = "test.contrib"

  final case class C(get: String)
  object C {
    import data._, typ.hlist._, typ.nat._
    implicit val DataC: Data[C] = Struct(
      "C",
      "get" -> empty[String] :+: HNil)(
      (x : C) => x.get:+:HNil)(
      (x: String:+:HNil) =>
      C(x.i[_0])
    )
  }

  object MyS3   extends S3("mys3", creds)
  object Bucket extends MyS3.Fragment[C, C](bucket)

  val mahTest = Property.forAll {
    val c1 = C("one")
    val c2 = C("two")

    val task = for {
      _ <- Bucket.insert(c1, c2)
      res <- Bucket.lookup(c1)
      _ <- Bucket.delete(c1)
    } yield res


    val \/-( res ) = unsafeRun(task)
    unsafe.println(res)

    true
  }

} //S3Spec
