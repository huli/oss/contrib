package contrib
package io

import scalaprops._
import prelude._, std._, test._
import std.instances.data._

object TrailDBSpec extends Scalaprops {

  final case class Book(buy: List[(Long, Long)], sell: List[(Long, Long)])

  implicit def DataBook: data.Data[Book] = data.Data.instance(
    b => data.DString(b.toString),
    _ => unsafe.??? )

  val FromBook = Trail.From[Unit -> Book] {
    case _ -> Book(buy, sell) =>
      val ts = Timestamp.raw

      // Each position in the book is a separate trail.
      val buys = buy.zipWithIndex map { case (p, v) -> idx =>
        val uuid   = Trail.unsafeUUID(s"buy_${idx}")
        val values = Array(p.toString, v.toString)
        val event  = (uuid, ts, values)
        event }

      val sells = sell.zipWithIndex map { case (p, v) -> idx =>
        val uuid   = Trail.unsafeUUID(s"sell_${idx}")
        val values = Array(p.toString, v.toString)
        val event  = (uuid, ts, values)
        event }

      buys ++ sells }

  val schema: Trail.Schema = Map(
    "books" -> (Array("price", "volume") -> Just(FromBook)) )

  val book1 = Book(List( (42L, 1L), (41L, 10L) ), List( (43L, 1L), (44L, 8L) ))
  val book2 = Book(List( (41L, 10L) ),            List( (43L, 1L), (44L, 7L), (45L, 2L) ))

//======================================================================
  val dir = java.lang.System.getProperty("user.dir") + "/target/tdbs"

  object MyTDB extends TrailDB("mytdb", dir, schema)
  object Books extends MyTDB.Fragment[Unit, Book]("books")

//======================================================================
  val tdbTest = Property.forAll {
    val task = for {
      _    <- Books.open
      _    <- Books.insert(unit, book1)
      _    <- Books.insert(unit, book2)
      _    <- Books.close
      res1 <- MyTDB.slurp("books")
    } yield res1

    val \/-(Just(bytes)) = unsafeRun(task)
    val hash = bytes |> net.netty.crypto.sha256 |> net.netty.crypto.hex
    unsafe.println(hash)
    true
  }

} //Diskspec
