package contrib
package net
package wamp

import scalaprops._
import prelude._, std._, test._
import prelude.net._, dsl._, netty.{ Address, EventLoop }
import prelude.data.Datum
import std.instances.data._
import data.instances.data._

object MySubscriber extends net.Subscriber {
  var events    = List.empty[Request[Datum]]
  val subscribe = Subscribe [Datum] {
    case Put upon Root / "mining" / "profitability_updates" =>
      req => Task0 { events = req::events } ~> Log.info("profitability.update")
  }
}

object MyWamp extends wamp.Subscriber(
  Address(
    "live.prohashing.com",
    443,
    //"resources/certs/live.prohashing.com.pem".just
  ),
  WAMP(
    uri    = "wss://live.prohashing.com:443/ws",
    realm  = "mining",
    authid = "web",
    topic  = "profitability_updates",
    secret = "web"),
  MySubscriber)

//======================================================================
object SubscriberSpec extends Scalaprops {

  val basicTest = Property.forAll {

    val task = for {
      _ <- MyWamp.start             ~> Log.info("start")
      _ <- Task0.sleep(Seconds(10)) ~> Log.info("sleep")
      _ <- MyWamp.stop              ~> Log.info("stop")
      _ <- EventLoop.shutdown       ~> Log.info("shutdown")
    } yield unit

    val res = unsafeRun( task )
    // MySubscriber.events.foreach(unsafe.println)
    !MySubscriber.events.isEmpty
  }

} //SubscriberSpec
