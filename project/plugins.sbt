addSbtPlugin("com.eed3si9n"          % "sbt-assembly"    % "1.2.0")
addSbtPlugin("pl.project13.scala"    % "sbt-jmh"         % "0.4.3")
addSbtPlugin("com.github.scalaprops" % "sbt-scalaprops"  % "0.4.3")
