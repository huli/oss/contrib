package contrib
package io

import prelude._, std._
import prelude.io._
import data.{ Data, Datum, DString, Format, format }
import net.{ Code, Response, Stringly }
import net.dsl._
import net.netty.{ Address, crypto, http }
import std.instances.data._
import data.instances.data._
import net.instances.data._

class S3
  (name: String, creds: S3Credentials)
  extends io.Device[S3.Query](name, new S3I(_, creds))

object S3 {
  type Query[K, V] = (K, V) => Boolean

  def Query[K, V](f: (K -> V) ?=> Boolean): Query[K, V] = {
    case kv if f isDefinedAt kv => f(kv)
    case _                      => false }
}

//======================================================================
final class S3I
  (device: String, creds: S3Credentials)(implicit ev: Format = format.Json)
  extends Interface[S3.Query](device)
{
  val client = new S3Client(creds)

  def key  [K: Data](k: K): String = Data[K].pickle(k) |> crypto.hex
  def unKey[K: Data](s: String): K = crypto.unHex(s) |> Data[K].unpickle

  override def open (db: String) = Task0.unit & Entry.nop
  override def close(db: String) = Task0.unit & Entry.nop
  override def drop (db: String) = Task0.unit & Entry.nop

  override def insert[K: Data, V: Data](b: String, k: K, v: V) =
    client.put(b, key[K](k), Data[V].pickle(v))

  override def lookup[K: Data, V: Data](b: String, k: K) =
    client.get(b, key[K](k)) map (_ map Data[V].unpickle)

  override def delete[K: Data](b: String, k: K) =
    client.delete(b, key[K](k))

  override def scan[K: Data, V: Data](b: String) =
    client.parScan(b) map (_ map { case k -> v =>
      unKey[K](k) -> Data[V].unpickle(v) } )

  // FIXME: use object lock
  override def transaction
    [K: Data, V: Data, A](b: String, k: K, t: T[A, V]): Task[A]
    = for {
      mv  <- lookup[K, V](b, k)
      amv <- t(mv)
      _   <- insert[K, V](b, k, unsafe.unwrap(amv._2)) if !amv._2.isEmpty
      _   <- delete[K](b, k) if !mv.isEmpty && amv._2.isEmpty
    } yield amv._1

  // FIXME: prefix!
  override def query[K: Data, V: Data](b: String, q: S3.Query[K, V]) =
    scan[K, V](b) map (_ filter q.tupled)
}

//======================================================================
final class S3Client
  (creds: S3Credentials)
  extends net.Client(
  new http.SecureClient(Address(creds.host, 443), Minutes(2)),
  packetDump = false)
{
  override def formats = format.Blob -> format.Blob

  val b = Symbol("bucket")
  val k = Symbol("key")

  val cp = creds.prepare _

  val PUT    = Call [Bytes, Datum, Unit]  (Put    upon Root / b / k)
    .rewrite(cp)
    .rateLimit(200)
  val GET    = Call [Bytes, Datum, Bytes] (Get    upon Root / b / k)
    .rewrite(cp)
    .rateLimit(200)
  val DELETE = Call [Bytes, Datum, Unit]  (Delete upon Root / b / k)
    .rewrite(cp)
  val LIST   = Call [Bytes, Datum, Datum] (Get    upon Root / b)
    .arg(Stringly("list-type", "2"))


//======================================================================
  def put(b: String, k: String, v: Bytes, retry: Retry = Retry()): Task[Unit] =
    (PUT(b, k)(v) map {
      case Response(Code.Ok(Empty()), _) => unit
      case rep @ Response(_, _)          => unsafe.abort("insert", rep)
     }).flatFold(
      res => Task0(res) & Entry.nop,
      exn => retry.bump(exn).go(put(b, k, v, _)) )

  def get(b: String, k: String, retry: Retry = Retry()): Task[Maybe[Bytes]] =
    (GET(b, k)() map {
      case Response(Code.NotFound(_), _)  => empty[Bytes]
      case Response(Code.Ok(Just(bs)), _) => bs.just
      case rep @ Response(_, _)           => unsafe.abort("lookup", rep)
     }).flatFold(
      res => Task0(res) & Entry.nop,
      exn => retry.bump(exn).go(get(b, k, _)) )

  def delete(b: String, k: String) = DELETE(b, k)() map {
    case Response(Code.Ok(Empty()), _) => unit
    case rep @ Response(_, _)          => unsafe.abort("delete", rep) }

  def list(b: String, ct: Maybe[String]): Task[Datum] = ct.cata(
    ct => LIST arg Stringly("continuation-token", ct), LIST)
    .rewrite(creds.prepare)(b)() map {
      case Response(Code.Ok(Just(bs)), _) => bs
      case rep @ Response(_, _)           => unsafe.abort("list", rep) }

//======================================================================
  def multilist
    (b: String, ct: Maybe[String] = empty, res: List[String] = nil)
    : Task[List[String]]
    = list(b, ct) flatMap { d =>
      val DString(body) = d
      val (keys, token) = S3Client.parseXml(body)
      if (token.isEmpty) Task0(res ++ keys) & Entry.nop
      else multilist(b, token, res ++ keys) }

  def multiget
    (b: String, ks: List[String], res: List[String -> Bytes] = nil)
    : Task[List[String -> Bytes]]
    = ks match {
      case Nil() => Task0(res) & Entry.nop
      case k::ks => get(b, k) flatMap { v =>
        val kv = k -> unsafe.unwrap(v)
        multiget(b, ks, kv::res) } }

  def scan(b: String): Task[List[String -> Bytes]] =
    multilist(b) flatMap (multiget(b, _))

  def parScan(b: String, connections: Int = 10): Task[List[String -> Bytes]] =
    multilist(b) flatMap { keys =>

      def split[A](as: List[A], n: Int): List[List[A]] =
        as splitAt n match {
          case chunk -> Nil() => chunk.list
          case chunk -> rest  => chunk::split(rest, n) }

      val n      = keys.length / connections
      val chunks = cond(n == 0, keys.list, split(keys, n))

      Task.foreachPar(chunks)(multiget(b, _)) map (_.flatten)
    }
}

object S3Client {
  def parseXml(s: String): (List[String], Maybe[String]) = {
    val xml  = scala.xml.XML.loadString(s)
    val keys = xml \\ "Key" map (_.text)
    val ct   = xml \\ "NextContinuationToken"
    (List fromList keys.toList, cond(ct.isEmpty, empty, ct.head.text.just)) }
}

//======================================================================
final case class Retry(trace: List[EXN] = nil, tries: Int = 0, max: Int = 5)
{
  def backoff = Millis(2**tries * 100)

  def bump(e: EXN) = copy(trace = e::trace, tries = tries + 1)

  def go[A](f: Retry => Task[A]): Task[A] =
    if (tries > max) Task.fail("retry", trace)
    else Task0.sleep(backoff) & Log.warn("retry")(tries) andThen f(this)
}

// S3
