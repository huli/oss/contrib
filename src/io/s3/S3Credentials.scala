package contrib
package io

import prelude._, std._
import data.{ Charset, format }
import net._, netty.{ crypto, http }, Header.{ Accept, Content }

final case class S3Credentials
  (region         : String,
   accessKeyId    : String,
   secretAccessKey: String)
{
  val host = s"s3-${region}.amazonaws.com"

  def prepare(req: Request[Bytes]): Request[Bytes] = {
    val now     = Timestamp.toString("yyyyMMdd'T'HHmmss'Z'")(Timestamp.now)
    val hash    = (req.body | "".getBytes) |> crypto.sha256 |> crypto.hex
    req
      .header(Stringly("x-amz-date",           now))
      .header(Stringly("x-amz-content-sha256", hash)) |> (req => req
      .header(Stringly("Authorization", authorization(req, now, hash)) ) ) }

  def authorization(req: Request[Bytes], now: String, hash: String): String =
    s"AWS4-HMAC-SHA256 Credential=${accessKeyId}/${scope}," +
    s"SignedHeaders=${signedHeaders(req)},"                 +
    s"Signature=${signature(req, now, hash)}"

  def scope = {
    val date = Timestamp.toString("yyyyMMdd")(Timestamp.now)
    s"${date}/${region}/s3/aws4_request" }

  def signedHeaders(req: Request[Bytes]): String =
    rawHeaders(req).map(fst).reduceLeftOption(_ + ";" + _).get //XXX

  def rawHeaders(req: Request[Bytes]): List[String -> String] =
    // c.f. net.Client.
    (req header Content(format.Blob) header Accept(format.Blob)) |>
    (http.Request.encodeHeaders(_, host)
       .map(_ mapFst (_.toLowerCase))
       // S3 sets to keep-alive, we set to close.
       .filterNot(_._1 == "connection")
       .sortBy(fst))

  def signature(req: Request[Bytes], now: String, hash: String): String =
    stringToSign(req, now, hash)       |>
    (crypto.hmacSha256(_, signingKey)) |>
    crypto.hex

  def stringToSign(req: Request[Bytes], now: String, hash: String): String = {
    val date  = now
    val creq  =
      canonicalRequest(req, hash) |>
      Charset.Utf8.bytes          |>
      crypto.sha256               |>
      crypto.hex
    s"AWS4-HMAC-SHA256\n${date}\n${scope}\n${creq}" }

  def canonicalRequest(req: Request[Bytes], hash: String): String = {
    val verb     = req.command.toString.toUpperCase
    val uri      = http.uriEncode(req.path)
      .replace("%2F", "/") //Fixup URIEncoder which uses form rules
      .replace("+", "%20")
      .replace("*", "%2A")
      .replace("%7E", "~")
    val qs       = http.uriEncode(req.args.sortBy(_.name))
    val cHeaders = canonicalHeaders(req)
    val sHeaders = signedHeaders(req)
    val payload  = hash
    s"${verb}\n${uri}\n${qs}\n${cHeaders}\n${sHeaders}\n${payload}" }

  def canonicalHeaders(req: Request[Bytes]): String =
    (rawHeaders(req) map {
      case k -> v => s"${k}:${v.trim}\n"
    }).foldLeft("")(_ + _)

  def signingKey: String = {
    val date   = Timestamp.toString("yyyyMMdd")(Timestamp.now)
    val fields = List(date, region, "s3", "aws4_request")
    val key    = "AWS4" + secretAccessKey
    hmac(fields, key) }

  def hmac(strs: List[String], firstKey: String): String =
    (firstKey /: strs) { (key, str) =>
      crypto.hmacSha256(str, key) |> Charset.Latin1.string }

} //S3Credentials
