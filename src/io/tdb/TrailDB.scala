// TODO: more atomicity? E.g. multiple events derived from a single
// value may be interleaved with events produced by concurrent callers.
package contrib
package io

import prelude._, std._
import data.Data
import io._, device.{ Disk, DiskUtil, RefMap }
import std.instances.data._

class TrailDB
  (name: String, chroot: String, schema: Trail.Schema)
  extends Device[TrailDB.Query](
  name,
  Interface.fromInterface0(new TrailDBI(chroot, schema), _) )
{
  // Read file as blob, for shipping to S3.
  def slurp(db: String) =
    Disk.lookup(chroot, s"${db}.tdb") ~>
    (bs => Log.info("traildb.slurp")(bs map (_.length) getOrElse 0))
}

object TrailDB {
  final case class Query[K, V]()
}

//======================================================================
final class TrailDBI
  (chroot: String, schema: Trail.Schema)
  extends Interface0[TrailDB.Query]
{
  def file      (db: String) = s"${db}.tdb"
  def fieldNames(db: String) = unsafe.unwrap(schema lookup db map fst)
  def encoder   (db: String) = unsafe.unwrap(schema lookup db map snd)
  val thread                 = WriterThread.start()
  val pointers               = RefMap.make[String, Long] |> Runtime.unsafeRun0

//======================================================================
  override def open(db: String) =
    pointers.get(db) flatMap0 (_.cata(_ => Task0.unit, doOpen(db)))

  def doOpen(db: String) =
    //tdb adds ".tdb" extension
    DiskUtil.Path(chroot, db)            map0
    (_.toString)                         flatMap0 (path =>
    DiskUtil.mkdirp(chroot)              andThen0
    tdb_async.open(path, fieldNames(db)) flatMap0 (ptr =>
    pointers.put(db, ptr) ) )

  override def close(db: String) =
    pointers.get(db) flatMap0 (_.cata(doClose(db), Task0.unit))

  def doClose(db: String)(ptr: Long) =
    tdb_async.close(ptr) andThen0 pointers.del(db)

  override def drop(db: String) =
    close(db) andThen0 Disk.delete(chroot, file(db))

  override def insert[K: Data, V: Data](db: String, k: K, v: V) =
    pointers get db map0 unsafe.unwrap flatMap0 { ptr =>
      val enc    = encoder(db) map (_.asInstanceOf[Trail.From[K -> V]])
      val from   = enc getOrElse Trail.fromDatum[K -> V]
      val events = from(k -> v)
      Task0.foreach(events) { case (id, ts, vs) =>
        tdb_async.add(ptr, id, ts, vs) } }
}

// TrailDB
