package contrib
package io

import prelude._, std.{ Timestamp => _, _ }, std.instances.data._
import data.{ Data, DList }, data.instances.data._

object Trail {
  type T         = List[Event]
  type Event     = (UUID, Timestamp, Values)
  type UUID      = Bytes
  type Timestamp = Long
  type Values    = Array[String]

  //                 DB       Field names      Unsafe encoder
  type Schema  = Map[String, (Array[String] -> Maybe[From[Any]])]
  type From[A] = A => T

  def From[A](from: From[A]): From[Any] = from.asInstanceOf[From[Any]]

  def fromDatum[A: Data]: From[A] = (a: A) =>
    Data[A].toDatum(a).asList map {
      case DList(id::ts::vs::Nil()) =>
        val uid = id.asBytes |> unsafeUUID
        (uid, ts.asLong, vs.asList.map(_.asString) |> toArray)
      case d => unsafe.abort("Trail.fromDatum", d) }

  def toArray[A: ClassTag](as: List[A]): Array[A] = {
    val arr = newArray[A](as.length)
    var i   = 0
    as foreach { a => arr(i) = a ; i += 1 }
    arr }

  def unsafeUUID(s: String): Bytes =
    unsafeUUID(s.getBytes)
  def unsafeUUID(bs: Bytes): Bytes = bs match {
    case bs if bs.length == 16 => bs
    case bs if bs.length >  16 => unsafe.abort("Trail.unsafeUUID", new String(bs))
    case bs                    => bs.resize(16) }

} //Trail
