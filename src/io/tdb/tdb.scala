package contrib
package io

import java.lang.System
import java.nio.ByteBuffer
import java.util.UUID
import scala.native
import prelude._, std._, std.instances.data._

class tdb {
  @native def consInitOpen
    (root: String, fields: Array[String]): Long

  @native def consFinalize
    (cons: Long): Int

  @native def consClose
    (cons: Long): Unit

  @native def consAdd
    (cons: Long, uuid: Array[Byte], timestamp: Long, values: Array[String]): Int

  @native def errorStr
    (errcode: Int): String
}

object tdb {
  private val native = {
    val filename = System.getProperty("user.dir") + "/lib/tdb.so"
    System.load(filename)
    new tdb
  }

  def open(root: String, fields: Array[String]): Long = {
    val cons = native.consInitOpen(root, fields)
    if (cons < 0) abort(cons.toInt)
    unsafe.assert(cons > 0, "tdb.open", cons)
    cons
  }

  def close(cons: Long): Unit = {
    val ret = native.consFinalize(cons)
    if (ret != 0) abort(ret)
    native.consClose(cons)
  }

  def add(cons: Long, values: Array[String]): Unit =
    add(cons, uuid, Timestamp.raw, values)
  def add(cons: Long, uuid: Bytes, timestamp: Long, values: Array[String]): Unit = {
    val ret = native.consAdd(cons, uuid, timestamp, values)
    if (ret != 0) abort(ret)
  }

  private def abort(errcode: Int) = {
    val msg = native.errorStr(errcode)
    unsafe.abort("tdb", msg)
  }

  def uuid: Bytes = {
    val u = UUID.randomUUID
    ByteBuffer
      .allocate(16)
      .putLong(u.getMostSignificantBits)
      .putLong(u.getLeastSignificantBits)
      .array
  }

} //tdb
