// Basic JSON-over-WebSocket w/ WAMP-CRA subscriber.
//
// http://wamp-proto.org/static/rfc/draft-oberstet-hybi-crossbar-wamp.html
package contrib
package net
package wamp

import prelude._, std._, net._, dsl._, netty._, ws._
import http.HttpResponse
import data.{ format, Datum }
import std.instances.data._
import data.instances.data._

final case class WAMP
  (uri   : String,
   realm : String,
   authid: String,
   topic : String,
   secret: String)
{
  def handler = InboundHandler[Any](read, handle, active)

  lazy val handshaker = newHandshaker(uri, "wamp.2.json")


  def active(ctx: Context): Unit =
    (handshaker handshake ctx.channel) |>
    complete("active")


  def handle(exn: EXN, ctx: Context) = report(exn)


  def read(msg: Any, ctx: Context): Unit =
    if (handshaker.isHandshakeComplete)
      msg.asInstanceOf[WebSocketFrame] |>
      (read1(_, ctx))
    else
      msg.asInstanceOf[HttpResponse]                |>
      (handshaker finishHandshake (ctx.channel, _)) |>
      (_ => (new TextWebSocketFrame(HELLO)))        |>
      (ctx.channel writeAndFlush _)                 |>
      complete("read")

  def read1(frame: WebSocketFrame, ctx: Context) =
    if (frame.isInstanceOf[PingWebSocketFrame])
      frame.asInstanceOf[PingWebSocketFrame]   <|
      (_.content.retain())                     |> //FIXME
      (f => new PongWebSocketFrame(f.content)) |>
      (ctx.channel writeAndFlush _)            |>
      complete("read1.pong")
    else if (frame.isInstanceOf[PongWebSocketFrame])
      ()
    else if (frame.isInstanceOf[TextWebSocketFrame])
      frame.asInstanceOf[TextWebSocketFrame].text |>
      (format.Json unstringify _)                 |> //TODO: just look at first 3 chars
      (read2(_, ctx))
    else
      report(EXN("wamp", s"${realm}.read1", frame.toString))

  def read2(datum: Datum, ctx: Context): Unit = datum.list(0).asLong match {
    case WELCOME =>
      new TextWebSocketFrame(SUBSCRIBE) |>
      (ctx.channel writeAndFlush _)     |>
      complete("read2.welcome")
    case SUBSCRIPTION =>
      ()
    case CRA =>
      datum.list(2).map("challenge").asString |>
      AUTHENTICATE                            |>
      (new TextWebSocketFrame(_))             |>
      (ctx.channel writeAndFlush _)           |>
      complete("read2.cra")
    case EVENT =>
      // pass on to SubscriberHandler
      val _ = ctx fireChannelRead Request[Bytes](
        command = Put,
        path    = Path fromString s"/${realm}/${topic}",
        args    = nil,
        headers = Headers() content Header.Content(format.Json),
        body    = format.Json.pickle(datum.list(4)).just )
    case _ =>
      report(EXN("wamp", s"${realm}.read2", datum))
  }

//======================================================================
  def complete(tag: String)(f: Future) = (f addListener listener(tag)) |> ignore

  def listener(tag: String) = FutureListener(
    f => (),
    f => f.cause |> (EXN(_)) |> report)

  def report(exn: EXN) =
    Program(Task0.fail(exn) & Entry.nop) |> Runtime.unsafeSpawn

//======================================================================
  val WELCOME      = 2
  val CRA          = 4
  val EVENT        = 36
  def HELLO        = s"""[1, "${realm}", { "roles"      : {"subscriber": {}}
                                         , "authmethods": ["wampcra"]
                                         , "authid"     : "${authid}"
                                         }]"""
  val AUTHENTICATE = (challenge: String) => s"""[5, "${sign(challenge)}", {}]"""
  def SUBSCRIBE    = s"""[32, ${crypto.rand(2**53)}, {}, "${topic}"]"""
  val SUBSCRIPTION = 33

  private def sign(challenge: String): String =
    crypto.hmacSha256(challenge, secret) |> crypto.asciiArmor0 |> (new String(_))

} //WAMP
